from datetime import datetime, timedelta, timezone
import os
import json
import sys
import time
import shutil
from os.path import dirname

import constants
import users
import files as file_utils

# TODO: allow multiple directories of users

is_production = input("Production [Y] or [N]: ")

# Using a timestamp from 2 weeks ago, maybe it will work
t1 = datetime.now(timezone.utc) # end time
t0 = t1 - timedelta(14)         # start time
dms_ts = int(t0.timestamp())

# Master user data
slack_users = ''
with open(os.path.join('default', constants.FILENAME_USERS)) as data_file:
    slack_users = json.load(data_file)

#users not in master user data
missing_speakers = []

# empty list for creating empty json files
empty = []

# listing of all found dms
dms = []

# listing of all found dms
mpims = []

# for creation of new channels
channels = []

# for creation of new groups
groups = []

dms_file = ''
mpims_file = ''
groups_file = ''
channels_file = ''
users_file = ''

for root, dirs, files in os.walk(constants.ROOT_DIR, topdown=False):
                
    # loop over all the directories        
    for dir in dirs:
        
        # copy in the default channels file
        # TODO: do this at the end
        if dir == 'Files':
            dest = os.path.join(root, dir, constants.FILENAME_CHANNELS)
            shutil.copy(f"default/{constants.FILENAME_CHANNELS}", dest)
            
        parent = os.path.basename(root)
        if parent == 'Files':

            # Create import json files
            dms_file = file_utils.touch(root, constants.FILENAME_DMS)
            mpims_file = file_utils.touch(root, constants.FILENAME_MPIMS)
            groups_file = file_utils.touch(root, constants.FILENAME_GROUPS)
            users_file = file_utils.touch(root, constants.FILENAME_USERS)
            
            # members of a conversation
            participants = []

            # find all files in a conversation directory
            convo_files = os.listdir(os.path.join(root, dir))
            
            # open each one
            for file in convo_files:
                src = os.path.join(root, dir, file)
                with open(src) as json_file:
                    convo_data = json.load(json_file)
                    
                    # for some reason these files have extra tags in the json, remove them 
                    content = ''
                    if 'messages' in convo_data:
                        content = convo_data['messages']
                    else:
                        content = convo_data
                    
                    ts = ''
                    count = 0
                    for message in content:
                        count = count + 1
                        
                        # use the first ts for the whole conversation
                        if count == 1:
                            ts = message['ts']
                        isBot = False
                        userID = ''
                        if 'bot_id' in message:
                            isBot = True
                            userID = message['bot_id']
                        elif 'user' in message:
                            userID = message['user']
                        
                        # If we have a valid ID - no bots allowed
                        if userID and not isBot:
                            
                            if not userID.startswith('B'):
                                # append to this files participants list
                                if userID not in participants:
                                    participants.append(userID)
                                
                                # look for participant in user data  
                                speaker_info = users.search(userID, slack_users)
                                
                                # no info for this user?
                                if not speaker_info and not isBot:
                                    
                                    # are they already in missing speakers for later testing
                                    if userID not in missing_speakers:
                                        missing_speakers.append(userID)
                                    
                                    # Aparently leaving the team_id blank is acceptible
                                    team_id = ''
                                    if 'team' in message:
                                        team_id = message['team']
                                        
                                    # ok, add these users to slack_users
                                    this_user = ''
                                    if 'user_profile' in message:
                                        this_user = {
                                            "id": message['user'],
                                            "team_id": team_id,
                                            "name": message['user_profile']['name'],
                                            "deleted": True,
                                            "is_bot": False,
                                            "is_app_user": False,
                                            "updated": int(time.time()),
                                            "is_restricted": False,
                                            "is_ultra_restricted": False,
                                        }
                                    elif 'user' in message:
                                        # These folks remain unknown...
                                        this_user = {
                                            "id": message['user'],
                                            "team_id": team_id,
                                            "name": "unknown.user",
                                            "deleted": True,
                                            "is_bot": False,
                                            "is_app_user": False,
                                            "updated": int(time.time()),
                                            "is_restricted": False,
                                            "is_ultra_restricted": False,
                                        }
                                    if this_user:   
                                        slack_users.append(this_user)
                
                if len(participants) > 1:           
                    # set up this dm    
                    dm = {
                        'id': dir,
                        'created': dms_ts,
                        'members': participants
                    }
                    
                    # trying a ts taken from a legit export
                    dms_ts = dms_ts + 1
                    
                    # Does this dm already exist?
                    dm_found = users.search(dm['id'], dms)
                    
                    if not dm_found:
                        # If not, go ahead and add it
                        dms.append(dm)
                    else:
                        # otherwise...
                        for dm in dms:
                            # Find this dm, and merge any new participants
                            if dm.get('id', 0) == dm_found['id']:
                                for participant in participants:
                                    if participant not in dm['members']:
                                        dm['members'] = participant
                file_utils.write_json(src, content)

# Let's do that ETL now, if we're ready to import into production
if is_production == 'Y':
    for index, user in enumerate(slack_users):
        if 'profile' in user:
            if 'email' in user['profile']:
                for email in constants.ETL_EMAIL_LIST:
                    if email['original'] == user['profile']['email']:
                        slack_users[index]['profile']['email'] = email['new']
                if constants.ETL_EMAIL_DOMAIN_ORIGINAL in user['profile']['email']:
                    print('found broadcom')
                    print(slack_users[index]['profile']['email'])
                    this_email = slack_users[index]['profile']['email']
                    this_email = this_email.replace(constants.ETL_EMAIL_DOMAIN_ORIGINAL, constants.ETL_EMAIL_DOMAIN_NEW)
                    slack_users[index]['profile']['email'] = this_email
                    print(slack_users[index]['profile']['email'])



# OK, let's go looking for dms with more that 2 members to add to mpims
for index, dm in enumerate(dms):
    if len(dm['members']) > 2:
        this_dm = dm   
        this_dm['is_archived'] = False
        this_dm['name'] = dm['id']
        this_dm['topic'] = {'value': '', 'creator': '', 'last_set': 0}
        
        # We need to designate a creator and purpose for this mpim
        creator = ''
        purpose = 'Group messaging with: '
        
        for member in dm['members']:
            
            # if this is a user and not a bot...
            if member.startswith('U') or member.startswith('W'):
                
                # get the member's info
                member_info = users.search(member, slack_users)

                # add their name to the purpose
                purpose += f"@{member_info['name']} "
                
                # and use the first available user as creator
                if creator == '':
                        creator = member_info['id']

        this_dm['creator'] = creator
        this_dm['purpose'] = {'value': purpose, 'creator': creator, 'last_set': dm['created']}

        # add to the mpims list
        mpims.append(this_dm)
        
        # remove this dm from dms
        del dms[index]

if not dms and not mpims:
    sys.exit('ERROR: no dms or mpims found')        

# Write the mpims.json file
file_utils.write_json(mpims_file, mpims)
                   
# Write the dms.json file
file_utils.write_json(dms_file, dms)

# Write the users.json file
file_utils.write_json(users_file, slack_users)       

# zip up the content directory and move it to ../output
for root, dirs, files in os.walk(constants.ROOT_DIR, topdown=False):
    depth = root[len(constants.ROOT_DIR) + len(os.path.sep):].count(os.path.sep)
    if depth == 3:
        src = root
        # TODO: Get name of archive earlier so it can be echoed in the no dms/mpims message
        name = os.path.split(os.path.split(dirname(dirname(dirname(root))))[1])[1]
        shutil.make_archive(name, 'zip', src)
        shutil.move(f"{name}.zip", f"output/{name}.zip")