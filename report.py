import os
import json
import math

import constants
import files as file_utils
import size as size_utils
import list as list_utils

channel_type = ''

# Data containers
channel_data = [];
user_data = []
channels_size = []

for root, dirs, files in os.walk(constants.ROOT_DIR, topdown=True):
    
    # Get the summed size of channel directories
    for dir in dirs:
        depth = root[len(constants.ROOT_DIR) + len(os.path.sep):].count(os.path.sep)
        if depth == 1:
            # This is the content directory of the archive
            this_path = os.path.join(root, dir)
            
            # Get the sum of the size of the files
            size = 0
            for ele in os.scandir(this_path):
                size+=os.path.getsize(ele)
                
            if list_utils.value_in_dict(channels_size, dir):
                # Directory is already in list - sum the sizes of the named directory
                current_size = size_utils.get(channels_size, dir)
                new_size = size_utils.set(channels_size, dir, size)
            else:
                # Directory is not in list - add it
                channels_size.append({'directory': dir, 'size':size})
                
    # Read JSON files for User and Channel info
    for file in files:
        if file == f"{constants.FILENAME_EXPORT_GROUPS}.json" or file == f"{constants.FILENAME_EXPORT_CHANNELS}.json":
            with open(os.path.join(root, file)) as data_file: 
                data = json.load(data_file)
                for group in data:
                    if file == f"{constants.FILENAME_EXPORT_GROUPS}.json":
                        channel_type = 'Private'
                        is_general = False
                    else:
                        channel_type = 'Public'
                        is_general = group['is_general']
                    this_group_data = (f"{group['name']}, {channel_type}, {group['is_archived']}, {is_general}, {group['id']}\n")
                    if this_group_data not in channel_data:
                        channel_data.append(this_group_data)
        elif file == f"{constants.FILENAME_EXPORT_USERS}.json":
            # Parse the users export
            with open(os.path.join(root, file)) as data_file: 
                data = json.load(data_file)
                for user in data:
                    is_active = not user['deleted']
                    
                    email = ''
                    if 'email' in user['profile']:
                        email = user['profile']['email']       

                    this_user_data = (f"{user['profile']['real_name']}, {user['name']}, {email}, {is_active}, {user['is_bot']}, {user['is_app_user']}, {user['is_restricted']}, {user['is_ultra_restricted']}, {user['id']}\n")
                                        
                    if this_user_data not in user_data:
                        user_data.append(this_user_data)


# Write channel size report
channels_size = sorted(channels_size, key = lambda i: i['size'], reverse=True)
print_data = f"{constants.FIELDS_CHANNELS_SIZE}\n"
for index in range(len(channels_size)):
    print_data += f"{channels_size[index]['directory']}, {size_utils.convert_size(channels_size[index]['size'])}\n"
f = open(f"{constants.ROOT_DIR}/{constants.FILENAME_EXPORT_CHANNELS}_size.csv", "w")
f.write(print_data)

# Write channel report
channel_data.sort()
file_utils.write_csv(constants.ROOT_DIR, constants.FIELDS_CHANNELS, channel_data)

# Write user report
user_data.sort()
file_utils.write_csv(constants.ROOT_DIR, constants.FIELDS_USERS, print_data)

