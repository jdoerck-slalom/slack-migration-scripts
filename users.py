import json
import os
import files as file_utils
import constants


def search(id, users):
    for u in users:
        if u['id'] == id:
            return u


def get_all(path_to_users):
    slack_users = []
    with open(os.path.join(path_to_users, "users.json")) as data_file:
        user_data = json.load(data_file)
        for user in user_data:
            slack_users.append({
                'id': user["id"],
                'name': user["profile"]["display_name"]
            })
    return slack_users

with open(
    os.path.join('default', constants.FILENAME_USERS)
) as data_file:
    slack_users = json.load(data_file)


for index, user in enumerate(slack_users):
    if 'profile' in user:
        if 'email' in user['profile']:
            for email in constants.ETL_EMAIL_LIST:
                if email['original'] == user['profile']['email']:
                    slack_users[index]['profile']['email'] = email['new']
            if constants.ETL_EMAIL_DOMAIN_ORIGINAL in user['profile']['email']:
                # print('found broadcom')
                # print(slack_users[index]['profile']['email'])
                this_email = slack_users[index]['profile']['email']
                this_email = this_email.replace(
                    constants.ETL_EMAIL_DOMAIN_ORIGINAL,
                    constants.ETL_EMAIL_DOMAIN_NEW
                )
                slack_users[index]['profile']['email'] = this_email
                # print(slack_users[index]['profile']['email'])

# Write the dms.json file
users_file = file_utils.touch('output', constants.FILENAME_USERS)
file_utils.write_json(users_file, slack_users)


# users_file = open("data/users.json", "rt")
# data = users_file.read()
# users_file.close()

# for email in constants.ETL_EMAIL_LIST:
#     data.replace(email['original'], email['new'])

# print(data)

# # fout = open("data/users_new.json", "wt")
# # fout.write(data)
# # fout.close()
