import json
import constants
from os.path import exists, abspath, join

def touch(the_path, filename):
    """Creates a file to read/write

    Parameters:
    the_path (str): Path to where the file should be created
    filename (str): Name of the file to be created, including filetype

    Returns:
    str: Path to the created file
    """
    
    the_path = abspath(the_path)  
    file_source = join(the_path, filename)
    file_exists = exists(file_source)
    if not file_exists:
        f = open(file_source, "w")
        empty = []
        f.write(json.dumps(empty, indent=4))
        f.close
    
    return file_source

def write_json(path, data):
    f = open(path, "w")
    f.write(json.dumps(data, indent=4, sort_keys=True))
    f.close

def write_csv(path, fields, data):
    data.sort()
    output = f"{fields}\n"
    for i in data:
        output += i
    f = open(f"{path}/{constants.FILENAME_EXPORT_CHANNELS}.csv", "w")
    f.write(output)
    f.close()

