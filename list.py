def value_in_dict(sample_dict, value):
    """Check if given value exists in list of dictionaries """
    for elem in sample_dict:
        if value in elem.values():
            return True
    return False