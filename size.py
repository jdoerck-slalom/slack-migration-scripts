def convert(size_bytes):
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return "%s %s" % (s, size_name[i])

def get(sample_dict, value):
    for elem in sample_dict:
        if value in elem.values():
            return elem['size']
    return False

def set(sample_dict, value, new_size):
    for elem in sample_dict:
        if value in elem.values():
            elem['size'] = elem['size'] + new_size
            return elem['size']
    return False