# Slack Migration Scripts

A collection of scripts for migrating a Slack workspace. These have been written specifically to migrate **from** Enterprise Grid **to** Business+ I'll need to organize better depending on future migrations. I started to make a single script to handle all functions, but the reality is, they need to be run so many times, its probably better to just generate a new constants each time. We'll see how many of these we end up doing and how much time we want to spend of the scripts.

There is a fair amount of cleanup that needs to happen here, but I don't want to keep continue referencing client data - once we get our own Grid workspace, we can get back into it.

## Setup

1. `cp constants.py.example constants.py` and update variables under `# MIGRATION SPECIFIC VALUES` to needed values
2. move export data into `data`
3. move latest `users.json` and `channels.json` into `default`



## Functions

### Bots

Bots use multiple formats for displaying data - plain text, markdown, block kit, attachmnents, etc. When importing, Slack only imports the text fields - so you end up with blank messages from bots. This script goes thru export data and, for channels in `constants.BOT_CONVERSATIONS`, pulls their messages, reformats them into plain text, and posts them back into channel with a timestamp offset (default 1 second), as you cant have two messages with the same ts.

#### Usage
`python3 bots.py`

### Prepend

Based on Powershell ETL scripts originally written by Kaveh Nahid <kavehn@slalom.com>.

This script prepends text and chosen delimiter on any directories in the data directory and rewrites `channels.json` to reference the new channel names. This script uses the future `app.py` endpoint, and allows user entry of content, so...

#### Usage

`python3 app.py`

`[P]repend channels:`  P

`String to prepend:`  new

`Delimiter:`  -


### DMs

If the export you are working with used eDiscovery to export user DMs, you will need to do a fair amount of ETL to format the data for import - despite how much the files look like a legitimate Slack export. This implementation in particular was written to work with the tool Relativity - YMMV with exports from other tools.

**Note:** There is the option to run ETL on email addresses as necessary - selecting **Production** when starting the script will run the process.

#### Usage

`python3 dms.py`

### Report

Generates Channel size report