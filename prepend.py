import os
import constants
import json
import shutil


# def do_action():
print('sdlkfjsd')
# Request the string to prepend
prepend_string = input("String to prepend: ")
delimiter = input("Delimiter: ")

for (root, dirs, files) in os.walk(constants.ROOT_DIR, topdown=True):
    if (root.count(os.sep) == 2) and (len(dirs) > 0):
        # TODO: Not renaming directories correctly
        # this needs to be fixed
        abspath = os.path.abspath(root)
        directory_list = os.listdir(abspath)
        for directory in directory_list:
            print(os.path.isdir)
            if os.path.isdir(os.path.join(abspath, directory)):
                new_directory = f"{prepend_string}{delimiter}{directory}"
                print(f"{directory} renamned {new_directory}")
                os.rename(
                    os.path.join(abspath, directory),
                    os.path.join(abspath, new_directory)
                )

    for file in files:
        if file == 'channels.json':
            src = os.path.join(root, file)
            dest = os.path.join(root, f"old-{file}")
            path = shutil.copyfile(src,dest)
            print(f"file created: {path}")
            
            with open(os.path.join(root, file)) as data_file:
                channels = json.load(data_file)
                for channel in channels:
                    # change channel name in each listing
                    channel['name'] = f"{prepend_string}{delimiter}{channel['name']}"
                
            f = open(os.path.join(root, file), "w")
            f.write(json.dumps(channels, indent=4, sort_keys=True))    
