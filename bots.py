import os
import json
import re

import constants
import users

# Need to offset the time so re-posting is possible
ts_offset = input("Timestamp offset in seconds (defaut: 1): ")
if not ts_offset:
    ts_offset = 1

# Get Users
slack_users = users.get_all(constants.DEFAULT_DATA)
        
channels = []
print_data = ''
content = ''

for conversation in constants.BOT_CONVERSATIONS:
    # initialize the conversation
    conversation_data = []
    conversation_raw = ''
    for root, dirs, files in os.walk(constants.ROOT_DIR, topdown=False):  
        for dir in dirs:
            if dir == conversation:
                paths = os.path.join(constants.ROOT_DIR, root, dir)
                for the_files in os.walk(paths):
                    path_to_file = os.path.join(the_files[0])
                    for file in the_files[2]:
                        with open(os.path.join(path_to_file, file)) as data_file:
                            conversation_raw = json.load(data_file)
                            # timestamp, channel, username, text
                            for post in conversation_raw:
                                if 'hidden' not in post:
                                    
                                    # Add an offset to timestamp to help moderate incorrectly imported messages
                                    ts = float(post['ts']) + ts_offset
                                    ts = format(ts, '.6f')

                                    channel = conversation
                                    text = ''
                                    link = ''
                                    title = ''
                                    
                                    user = ''
                                    if 'user' in post:
                                        if users.search(post['user'], slack_users):
                                            lookup_user = users.search(post['user'], slack_users)
                                            if lookup_user['name'] != '':
                                                user = lookup_user['name']
                                        else:
                                            user = post['user']
                                    elif 'username' in post:
                                        user = post['username']
                                    elif 'bot_id' in post:
                                        user = 'bot user'
                                    
                                    
                                    if "text" in post:
                                        if post['text'] != "This content can't be displayed.":
                                            text = f"{post['text']} " 
                                    if 'blocks' in post.keys():
                                        for block in post['blocks']:
                                            if 'fields' in block.keys():
                                                for field in block['fields']:
                                                    text += f"{field['text']} "
                                            if 'elements' in block.keys():
                                                    for element in block['elements']:
                                                        if element['type'] == 'text':
                                                            text += element['text']
                                            if 'text' in block.keys():
                                                    text += f"{block['text']['text']} "   
                                    if "attachments" in post:
                                        for attachment in post['attachments']:
                                            if 'title' in attachment:
                                                title = attachment['title']
                                                if 'title_link' in attachment:
                                                    if attachment['title_link'] != "https:\/\/console.cloud.google.com\/monitoring\/alerting\/policies\/15710678486204377212?project=esd-blazect-svcs-prd":
                                                        link = attachment['title_link']
                                                if link != '':
                                                    title = f"{title}"
                                            text += f"{title}"
                                                
                                            if 'text' in attachment:
                                                text += f"{attachment['text']}"
                                            if 'pretext' in attachment:
                                                this_pretext = ''
                                                if conversation == 'BM-aha-ideas':
                                                    this_pretext = attachment['pretext'].replace('<https', ' https')
                                                    this_pretext = this_pretext.replace("|", ' ')
                                                text += f"{this_pretext}"
                                            if 'fields' in attachment:
                                                # text += '\n'
                                                for field in attachment['fields']:
                                                    text += f"*{field['title']}:* {field['value']}"

                                    # Replace @mentions
                                    pattern = '<@[A-Z0-9]*>'
                                    user_mentions = re.findall(pattern, text)
                                    for mention in user_mentions:
                                        if users.search(mention[2:13], slack_users):
                                            lookup_user = users.search(post['user'], slack_users)
                                            if lookup_user['name'] != '':
                                                replace = lookup_user['name']
                                            else:
                                                replace = "[[ User not found ]]"
                                            
                                            text = text.replace(mention, replace)

                                    text = text.replace(",", "")
                                    text = text.replace("'", "\'")
                                    # TODO: I think strip takes care of these. Test.
                                    text = text.replace("\n", " ")
                                    text = text.replace("\r", " ")
                                    text = text.replace("\t", " ")
                                    text = text.replace("Google Cloud Platform lets you build\, deploy\, and scale applications\, websites\, and services on the same infrastructure as Google.", " ")
                                    text = text.replace("```", "")
                                    stripped_text = text.strip()                                                 

                                    if text == '':
                                        text = 'no text'
                                    conversation_data.append(f"{ts}, {channel}, {user}, {stripped_text}")
    # Sort by timestamp
    conversation_data.sort()          
    for i in conversation_data:
        print_data += f"{i}\n"
        
    f = open(f"{constants.ROOT_DIR}/{conversation}.csv", "w")
    f.write(print_data)
    